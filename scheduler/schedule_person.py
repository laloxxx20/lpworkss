import datetime
import time

class SchedulePerson:

	def __init__(self):
		self.dict_times_real=dict()
		self.dict_times_int=dict()		

	def castStringTimetoInt(self,string_time): #rpta of youtube-dl about the time is a string, here we make a casting in answer
	    x = time.strptime(string_time,'%M:%S')
	    time_in_seconds=datetime.timedelta(minutes=x.tm_min,seconds=x.tm_sec).total_seconds()    
	    return time_in_seconds	

	def read_schedule(self,file):
		f=open(file,'r')
		data=f.readlines()
		# print data
		
		for line in data:
			dataOfLine = line.split() #split each line of file in words "mon", "00:00" ..
			# print dataOfLine
			day=dataOfLine[0] #saving the of line
			del dataOfLine[0] # deleting day of line, now list only with times
			self.dict_times_real[day]= dataOfLine #making dictionary with realTimes

			#(S)here part we split a normal line of times in "int times"
			listMatrixTime=[]
			for x in dataOfLine:				
				toListMatrixTime=x.split("-") ## split one time in a vector of two times to make it a "int"
				listMatrixTime.append(toListMatrixTime) # adhering list of pairs to listglobal of times

			# print listMatrixTime	 #-->[['08:30', '10:30'], ['14:03', '16:00'], ['17:10', '18:10']]
			listMatrixTimesInts=[]
			for pairTimes in listMatrixTime:
				pairTimeList=[]
				for time in pairTimes:					
					valuePairTimeList=self.castStringTimetoInt(time)					
					pairTimeList.append(valuePairTimeList)

				listMatrixTimesInts.append(pairTimeList)

			# print listMatrixTimesInts # -->[[510.0, 630.0], [843.0, 960.0], [1030.0, 1090.0]]
			self.dict_times_int[day]=listMatrixTimesInts #setting times "int " to self dict of "int's"
		# print self.dict_times_int
		# print self.dict_times_real
		
		
			#(E)here part we split a normal line of times in "int times"	

			


