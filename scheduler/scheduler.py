from schedule_person import *
import sys

class Scheduler:
	def __init__(self,time,scheduleOne,scheduleTwo):		
		self.scheduleOne=SchedulePerson() 
		self.scheduleOne.read_schedule(scheduleOne)

		self.scheduleTwo=SchedulePerson() 
		self.scheduleTwo.read_schedule(scheduleTwo)		

		self.timeInt=self.scheduleOne.castStringTimetoInt(time)
		self.weekDays=["mon","tue","wed","thu","fri"]
		self.answer=dict()

	def search(self,day,dictOne,dictTwo):
		dictOne[day]
		return 

	def castIntToStringTime(self, timeInt):
		return time.strftime('%M:%S', time.gmtime(timeInt))

	def getIntersection(self,a, b):
		return max(0, min(a[1], b[1]) - max(a[0], b[0]))	
		

	def compare(self):
		for day in self.weekDays:
			try:
				matrixOfInts=self.scheduleOne.dict_times_int[day]
			except KeyError:
				continue

			if matrixOfInts:				
				matrixAnswerTimes=[]	
				for pair in matrixOfInts:
					try:
						matrixOfIntsSecond=self.scheduleTwo.dict_times_int[day] #selecting second file data and return list of times second
					except KeyError:
						continue	

					for pairOfMatrixSecond in matrixOfIntsSecond:
						pairTimes=[]												

						#(S)here we choose the minor value between first elements
						if pair[0]>pairOfMatrixSecond[0]: 
							pairTimes.append(pair[0])
						else:	
							pairTimes.append(pairOfMatrixSecond[0])
						# print "choceOne: ",self.castIntToStringTime( pairTimes[0] ) #value minor chosen
						#(E)here we choose the minor value between first elements									

						#(S)here we choose the minor value between second elements
						if pair[1]<pairOfMatrixSecond[1]:
							pairTimes.append(pair[1])
						else:	
							pairTimes.append(pairOfMatrixSecond[1])	
						# print "choceTwo: ",self.castIntToStringTime( pairTimes[1])  #value mayor chosen
						#(E)here we choose the minor value between second elements
												
						intersecFirstCase=0
						intersecSecondCase=0					
						#(S)value	of intersections in two cases
						if self.getIntersection(pair,pairOfMatrixSecond)>0:
							intersecFirstCase=self.getIntersection(pair,pairOfMatrixSecond)							

						if self.getIntersection(pairOfMatrixSecond,pair)>0:	
							intersecSecondCase=self.getIntersection(pairOfMatrixSecond,pair)							
						#(E)value	of intersections in two cases	

						valueOfTimeProposed=abs(pairTimes[0]-pairTimes[1]) # testing if tha difference is close to timeInt						
						if valueOfTimeProposed>=self.timeInt and (intersecFirstCase>0 or intersecSecondCase >0):							
							pairTimes[0]=self.castIntToStringTime(pairTimes[0])
							pairTimes[1]=self.castIntToStringTime(pairTimes[1])
							matrixAnswerTimes.append(pairTimes)	
							self.answer[day]=matrixAnswerTimes

		# print self.answer

	def printAnswer(self):
		for day,times in self.answer.items():					
			sys.stdout.write(day+" ")
			for time in times:
				dayTimes=time[0]+"-"+time[1]+" "
				sys.stdout.write(dayTimes)
			sys.stdout.write("\n")	




						

		
